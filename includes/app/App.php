<?php

class App

{
	public function openConnection()
	{
		$connection = mysqli_connect('localhost', 'root', 'aurora', 'develop_blog');
		return $connection;
	}

	public function runQuery($conn, $sql)
	{
		$result = mysqli_query($conn, $sql);
		return $result;
	}

	public function closeConnection($conn)
	{
		mysqli_close($conn);
	}
}

?>