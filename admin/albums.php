<?php

	error_reporting(E_ALL & ~E_NOTICE);
	include_once "../includes/inc.admin.php";
	include "../includes/app/App.php";
	
	if(isset($_POST['addAlbum'])){

		$albumName = $_POST['albumName'];
		$albumDescription = $_POST['albumDescription'];
		$status = $_POST['status'];

		$albumNameError = '';
		$albumDescriptionError = '';
		$statusError = '';

		if(empty($albumName)){
			$albumNameError = 'Album name can not be empty';
			$smarty->assign("albumNameError", $albumNameError);
		}

		if(empty($albumDescription)){
			$albumDescriptionError = 'Album description can not be empty';
			$smarty->assign("albumDescriptionError", $albumDescriptionError);
		}

		if(empty($status)){
			$statusError = 'Album status must be checked';
			$smarty->assign("statusError", $statusError);
		}

		if($albumName && $albumDescription && $status){
			$appInsert = new App();
			$connInsert = $appInsert->openConnection();
			$insertSql = "INSERT INTO albums(name, description, status) VALUES('".$albumName."', '".$albumDescription."', '".$status."')";
			$runInsert = $appInsert->runQuery($connInsert, $insertSql);
			$smarty->assign("albumCreated", "Album created");
			$appInsert->closeConnection($connInsert);
		}
	}

	if(isset($_POST['deleteAlbum'])){
		$albumId = $_POST['albumId'];
		$appDelete = new App();
		$connDelete = $appDelete->openConnection();
		$deleteSql = "DELETE FROM albums WHERE id='".$albumId."'";
		$runDelete = $appDelete->runQuery($connDelete, $deleteSql);
		$smarty->assign("albumDeleted", "Album deleted");
		$appDelete->closeConnection($connDelete);
	}

	if(isset($_POST['updateAlbum'])){
		$albumId = $_POST['albumId'];
		$_SESSION['albumId'] = $albumId;
		$appUpdate = new App();
		$connUpdate = $appUpdate->openConnection();
		$updateSql = "SELECT * FROM albums WHERE id='".$albumId."'";
		$runUpdate = $appUpdate->runQuery($connUpdate, $updateSql);

		while($albumData = mysqli_fetch_object($runUpdate)){
			$aName = $albumData->name;
			$aDescription = $albumData->description;
			$aStatus = $albumData->status;
		}

		$smarty->assign("aName", $aName);
		$smarty->assign("aDescription", $aDescription);
		$smarty->assign("aStatus", $aStatus);
		$smarty->assign("check", "checked");

		$appUpdate->closeConnection($connUpdate);
	}

	if(isset($_POST['editAlbum'])){

		$albumId = $_SESSION['albumId'];

		$albumNameData = $_POST['albumName'];
		$albumDescriptionData = $_POST['albumDescription'];
		$albumStatusData = $_POST['status'];

		$appEdit = new App();
		$connEdit = $appEdit->openConnection();
		$editSql = "UPDATE albums SET name='".$albumNameData."', description='".$albumDescriptionData."', status='".$albumStatusData."' WHERE id='".$albumId."'";
		$runEdit = $appEdit->runQuery($connEdit, $editSql);
		$smarty->assign("albumUpdated", "Album updated");
		$appEdit->closeConnection($connEdit);

	}

	$appShow = new App();
	$connShow = $appShow->openConnection();
	$showSql = "SELECT * FROM albums";
	$runShow = $appShow->runQuery($connShow, $showSql);
	$smarty->assign("albums", $runShow);
	$appShow->closeConnection($connShow);

	$smarty->assign("admin", $admin);			
	$smarty->display("admin/albums.tpl");

?>