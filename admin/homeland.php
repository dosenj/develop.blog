<?php

	error_reporting(E_ALL & ~E_NOTICE);
	include_once "../includes/inc.admin.php";
	include "../includes/app/App.php";

	$app = new App();
	$connect = $app->openConnection();
	$sql = "SELECT * FROM sliders";
	$images = $app->runQuery($connect, $sql);
	$smarty->assign("images", $images);
	$app->closeConnection($connect);

	$appNavLinks = new App();
	$conn = $appNavLinks->openConnection();
	$navSql = "SELECT * FROM pages WHERE status='true'";
	$links = $appNavLinks->runQuery($conn, $navSql);
	$smarty->assign("links", $links);
	$appNavLinks->closeConnection($conn);

	$appPosts = new App();
	$connPosts = $appPosts->openConnection();
	$postsSql = "SELECT * FROM posts WHERE status='true'";
	$posts = $appPosts->runQuery($connPosts, $postsSql);
	$smarty->assign("posts", $posts);
	$appPosts->closeConnection($connPosts);

	$appImages = new App();
	$connImages = $appImages->openConnection();
	$imagesSql = "SELECT * FROM images WHERE status='true'";
	$allImages = $appImages->runQuery($connImages, $imagesSql);
	$smarty->assign("allImages", $allImages);
	$appImages->closeConnection($connImages);

	$editorPosts = new App();
	$editorConn = $editorPosts->openConnection();
	$editorSql = "SELECT * FROM editorposts WHERE status='true'";
	$runEditorPosts = $editorPosts->runQuery($editorConn, $editorSql);
	$smarty->assign("editorPosts", $runEditorPosts);
	$editorPosts->closeConnection($editorConn);

	$smarty->assign("admin", $admin);			
	$smarty->display("admin/homeland.tpl");

?>