<?php

	error_reporting(E_ALL & ~E_NOTICE);
	include_once "../includes/inc.admin.php";
	include "../includes/app/App.php";

	if(isset($_POST['addPost'])){

		$postTitle = $_POST['title'];
		$postContent = $_POST['content'];
		$postStatus = $_POST['status'];

		$postTitleError = '';
		$postContentError = '';

		if(empty($postTitle)){
			$postTitleError = 'Post title can not be empty';
			$smarty->assign("titleError", $postTitleError);
		}
		if(empty($postContent)){
			$postContentError = 'Post content can not be empty';
			$smarty->assign("contentError", $postContentError);
		}

		$fileName = basename($_FILES['image']['name']);
		$fileTempName = $_FILES['image']['tmp_name'];
		$fileSize = $_FILES['image']['size'];
		$location = "C:\\xampp\htdocs\develop.blog\uploads\\".$fileName;
		$fileType = strtolower(pathinfo($location, PATHINFO_EXTENSION));

		if($fileSize > 500000){
			$fileSizeError = 'File is to large';
			$smarty->assign("fileSizeError", $fileSizeError);
		}

		if(file_exists($location)){
			$fileError = 'File already exists';
			$smarty->assign("fileError", $fileError);
		}

		if($fileType != 'jpg' && $fileType != 'png' && $fileType != 'jpeg'){
			$fileTypeError = 'Only jpg, png and jpeg files are allowed';
			$smarty->assign("fileTypeError", $fileTypeError);
		} else {
			$userId = $logged_user->getUserId();
			$app = new App();
			$connection = $app->openConnection();
			$insertPost = "INSERT INTO posts(user_id, title, content, status, image, post_date, post_time) 
						   VALUES('".$userId."', '".$postTitle."', '".$postContent."', '".$postStatus."', '".$fileName."', CURDATE(), CURTIME())";
			$query = $app->runQuery($connection, $insertPost);
			$app->closeConnection($connection);
			move_uploaded_file($fileTempName, $location);
			$smarty->assign("message", "Post created");
		}
	}

	if(isset($_POST['deletePost'])){
		$postId = $_POST['postId'];
		$appDeletePost = new App();
		$connDeletePost = $appDeletePost->openConnection();
		$deletePostSql = "DELETE FROM posts WHERE id='".$postId."'";
		$runDeletePost = $appDeletePost->runQuery($connDeletePost, $deletePostSql);
		$appDeletePost->closeConnection($connDeletePost);
		$smarty->assign("postDeleted", "Post deleted");
	}

	if(isset($_POST['updatePost'])){
		$postId = $_POST['postId'];
		$_SESSION['postId'] = $postId;
		$appUpdatePost = new App();
		$connUpdatePost = $appUpdatePost->openConnection();
		$updatePostSql = "SELECT * FROM posts WHERE id='".$postId."'";
		$runUpdatePost = $appUpdatePost->runQuery($connUpdatePost, $updatePostSql);

		while($postData = mysqli_fetch_assoc($runUpdatePost)){
			$postTitleData = $postData['title'];
			$postContentData = $postData['content'];
			$postStatusData = $postData['status'];
		}

		$smarty->assign("postTitleUpdate", $postTitleData);
		$smarty->assign("postContentUpdate", $postContentData);
		$smarty->assign("postStatusUpdate", $postStatusData);
		$smarty->assign("check", "checked");

		$appUpdatePost->closeConnection($connUpdatePost);
	}

	if(isset($_POST['editPostData'])){

		$postId = $_SESSION['postId'];

		$pTitle = $_POST['title'];
		$pContent = $_POST['content'];
		$pStatus = $_POST['status'];

		$appEditPost = new App();
		$connEditPost = $appEditPost->openConnection();
		$postEditSql = "UPDATE posts SET title='".$pTitle."', content='".$pContent."', status='".$pStatus."', post_date=CURDATE(), post_time=CURTIME() WHERE id='".$postId."'";
		$runEditSql = $appEditPost->runQuery($connEditPost, $postEditSql);
		$appEditPost->closeConnection($connEditPost);
		$smarty->assign("info", "Post updated");
	}

	$appAllPosts = new App();
	$connect = $appAllPosts->openConnection();
	$sqlAllPosts = "SELECT * FROM posts";
	$allPosts = $appAllPosts->runQuery($connect, $sqlAllPosts);
	$smarty->assign("posts", $allPosts);
	$appAllPosts->closeConnection($connect);

	$smarty->assign("admin", $admin);			
	$smarty->display("admin/posts.tpl");
?>