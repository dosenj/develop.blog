<?php

	error_reporting(E_ALL & ~E_NOTICE);
	include_once "../includes/inc.admin.php";
	include "../includes/app/App.php";

	if(isset($_POST['addImageData'])){

		$imageName = $_POST['filename'];
		$imageDescription = $_POST['description'];
		$imageStatus = $_POST['status'];
		$selectedAlbum = $_POST['selectedAlbum'];

		$imageNameError = '';
		$imageDescriptionError = '';
		$imageStatusError = '';

		if(empty($imageName)){
			$imageNameError = 'Image name can not be empty';
			$smarty->assign("imageNameError", $imageNameError);
		}

		if(empty($imageDescription)){
			$imageDescriptionError = 'Image description can not be empty';
			$smarty->assign("imageDescriptionError", $imageDescriptionError);
		}

		if(empty($imageStatus)){
			$imageStatusError = 'Image status can not be empty';
			$smarty->assign("imageStatusError", $imageStatusError);
		}

		$fileName = basename($_FILES['image']['name']);
		$fileTempName = $_FILES['image']['tmp_name'];
		$fileSize = $_FILES['image']['size'];
		$location = "C:\\xampp\htdocs\develop.blog\uploads\\images\\".$fileName;
		$fileType = strtolower(pathinfo($location, PATHINFO_EXTENSION));

		if($fileSize > 500000){
			$fileSizeError = 'File is to large';
			$smarty->assign("fileSizeError", $fileSizeError);
		}

		if(file_exists($location)){
			$fileError = 'File already exists';
			$smarty->assign("fileError", $fileError);
		}

		if($fileType != 'jpg' && $fileType != 'png' && $fileType != 'jpeg'){
			$fileTypeError = 'Only jpg, png and jpeg files are allowed';
			$smarty->assign("fileTypeError", $fileTypeError);
		} else {
			$appInsert = new App();
			$connInsert = $appInsert->openConnection();
			$insertSql = "INSERT INTO images(album_id, name, description, status, image) 
						  VALUES('".$selectedAlbum."', '".$imageName."', '".$imageDescription."', '".$imageStatus."', '".$fileName."')";
			$runInsert = $appInsert->runQuery($connInsert, $insertSql);
			$smarty->assign("message", "Image created");
			move_uploaded_file($fileTempName, $location);
			$appInsert->closeConnection($connInsert);
		}

	}

	if(isset($_POST['deleteImageData'])){

		$imageId = $_POST['imageId'];

		$appDelete = new App();
		$connDelete = $appDelete->openConnection();
		$deleteSql = "DELETE FROM images WHERE id='".$imageId."'";
		$runDelete = $appDelete->runQuery($connDelete, $deleteSql);
		$smarty->assign("imageDeleted", "Image deleted");
		$appDelete->closeConnection($connDelete);

	}

	if(isset($_POST['updateImageData'])){

		$imageId = $_POST['imageId'];
		$_SESSION['imageId'] = $imageId;
		$appUpdate = new App();
		$connUpdate = $appUpdate->openConnection();
		$updateSql = "SELECT * FROM images WHERE id='".$imageId."'";
		$runUpdate = $appUpdate->runQuery($connUpdate, $updateSql);

		while($img = mysqli_fetch_object($runUpdate)){

			$albumId = $img->album_id;
			$a = new App();
			$c = $a->openConnection();
			$q = "SELECT * FROM albums WHERE id='".$albumId."'";
			$r = $a->runQuery($c, $q);

			while($rez = mysqli_fetch_object($r)){
				$albumName = $rez->name;
			}

			$iName = $img->name;
			$iDescription = $img->description;
			$iStatus = $img->status;
		}

		$smarty->assign("iName", $iName);
		$smarty->assign("iDescription", $iDescription);
		$smarty->assign("iStatus", $iStatus);
		$smarty->assign("albumName", $albumName);
		$smarty->assign("check", "checked");
		$smarty->assign("select", "selected");

	}

	if(isset($_POST['editImageData'])){

		$imageId = $_SESSION['imageId'];

		$imgName = $_POST['filename'];
		$imgDescription = $_POST['description'];
		$imgStatus = $_POST['status'];
		$imgAlbumId = $_POST['selectedAlbum'];

		$appEdit = new App();
		$connEdit = $appEdit->openConnection();
		$editSql = "UPDATE images SET album_id='".$imgAlbumId."', name='".$imgName."', description='".$imgDescription."', status='".$imgStatus."' WHERE id='".$imageId."'";
		$runEdit = $appEdit->runQuery($connEdit, $editSql);
		$smarty->assign("infoMessage", "Image updated");
		$appEdit->closeConnection($connEdit);

	}

	$showImages = new App();
	$connShowImages = $showImages->openConnection();
	$showImageSql = "SELECT * FROM images";
	$imagesData = $showImages->runQuery($connShowImages, $showImageSql);
	$smarty->assign("images", $imagesData);
	$showImages->closeConnection($connShowImages);

	$getAlbums = new App();
	$connAlbums = $getAlbums->openConnection();
	$albumsSql = "SELECT * FROM albums";
	$albumsData = $getAlbums->runQuery($connAlbums, $albumsSql);
	$smarty->assign("albums", $albumsData);
	$getAlbums->closeConnection($connAlbums);

	$smarty->assign("admin", $admin);			
	$smarty->display("admin/images.tpl");

?>