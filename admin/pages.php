<?php

	error_reporting(E_ALL & ~E_NOTICE);
	include_once "../includes/inc.admin.php";
	include "../includes/app/App.php";

	if(isset($_POST['addPage'])){

		$pageTitle = $_POST['title'];
		$pageContent = $_POST['content'];
		$pageStatus = $_POST['status'];

		$pageTitleError = '';
		$pageContentError = '';
		$pageStatusError = '';

		if(empty($pageTitle)){
			$pageTitleError = 'Page title can not be empty';
			$smarty->assign("pageTitleError", $pageTitleError);
		}
		if(empty($pageContent)){
			$pageContentError = 'Page content can not be empty';
			$smarty->assign("pageContentError", $pageContentError);
		}
		if(empty($pageStatus)){
			$pageStatusError = 'Page status must be checked';
			$smarty->assign("pageStatusError", $pageStatusError);
		}

		if($pageTitle && $pageContent && $pageStatus){
			$appInsert = new App();
			$connection = $appInsert->openConnection();
			$insertPageSql = "INSERT INTO pages(title, content, status, page_time) VALUES('".$pageTitle."', '".$pageContent."', '".$pageStatus."', CURTIME())";
			$insertQuery = $appInsert->runQuery($connection, $insertPageSql);
			$smarty->assign("pageCreated", "Page created");
			$appInsert->closeConnection($connection);
		}
	}

	if(isset($_POST['deletePage'])){
		$pageId = $_POST['pageId'];
		$appDeletePage = new App();
		$connDeletePage = $appDeletePage->openConnection();
		$deletePageSql = "DELETE FROM pages WHERE id='".$pageId."'";
		$runDeletePageQuery = $appDeletePage->runQuery($connDeletePage, $deletePageSql);
		$smarty->assign("delMessage", "Page deleted");
		$appDeletePage->closeConnection($connDeletePage);
	}

	if(isset($_POST['updatePage'])){
		$pageId = $_POST['pageId'];
		$_SESSION['pageId'] = $pageId;
		$findPage = new App();
		$conn = $findPage->openConnection();
		$sql = "SELECT * FROM pages WHERE id='".$pageId."'";
		$run = $findPage->runQuery($conn, $sql);

		while($page = mysqli_fetch_assoc($run)){
			$pTitle = $page['title'];
			$pContent = $page['content'];
			$pStatus = $page['status'];
		}

		$smarty->assign("pTitle", $pTitle);
		$smarty->assign("pContent", $pContent);
		$smarty->assign("pStatus", $pStatus);
		$smarty->assign("check", "checked");

		$findPage->closeConnection($conn);
	}

	if(isset($_POST['editPage'])){

		$pageId = $_SESSION['pageId'];

		$pageTitleData = $_POST['title'];
		$pageContentData = $_POST['content'];
		$pageStatusData = $_POST['status'];

		$editPageApp = new App();
		$editConn = $editPageApp->openConnection();
		$editPageSql = "UPDATE pages SET title='".$pageTitleData."', content='".$pageContentData."', status='".$pageStatusData."', page_time=CURTIME() WHERE id='".$pageId."'";
		$runEditQuery = $editPageApp->runQuery($editConn, $editPageSql);
		$smarty->assign("pageUpdated", "Page updated");
		$editPageApp->closeConnection($editConn);
	}

	$allPages = new App();
	$connect = $allPages->openConnection();
	$allPagesSql = "SELECT * FROM pages";
	$pagesData = $allPages->runQuery($connect, $allPagesSql);
	$smarty->assign("pages", $pagesData);
	$allPages->closeConnection($connect);

	$smarty->assign("admin", $admin);			
	$smarty->display("admin/pages.tpl");

?>