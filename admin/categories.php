<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include_once "../includes/inc.admin.php";
	include "../includes/app/App.php";
	
	if(isset($_POST['add'])){
		$categoryName = $_POST['name'];
		$categoryStatus = $_POST['status'];
		$categoryNameError = '';
		if(empty($categoryName)){
			$categoryNameError = 'Category name can not be empty';
			$smarty->assign("error", $categoryNameError);
		}
		if($categoryName == true && $categoryStatus == true){
			$app = new App();
			$connect = $app->openConnection();
			if($connect){
				$insertRecord = "INSERT INTO categories(name, status) VALUES('".$categoryName."', '".$categoryStatus."')";
				$query = $app->runQuery($connect, $insertRecord);
				$app->closeConnection($connect);
				$smarty->assign("message", "Category created");
			} else {
				$connError = 'Could not connect to database';
				$smarty->assign("connError", $connError);
			}
		}
	}

	if(isset($_POST['delete'])){
		$recordId = $_POST['recordId'];
		$appDelete = new App();
		$connDelete = $appDelete->openConnection();
		$recordDelete = "DELETE FROM categories WHERE id='".$recordId."'";
		$runDelete = $appDelete->runQuery($connDelete, $recordDelete);
		$smarty->assign("categoryDeleted", "Category deleted");
		$appDelete->closeConnection($connDelete);
	}

	if(isset($_POST['update'])){
		$recordId = $_POST['recordId'];
		$_SESSION['recId'] = $recordId;
		$appUpdate = new App();
		$connUpdate = $appUpdate->openConnection();
		$recordUpdate = "SELECT * FROM categories WHERE id='".$recordId."'";
		$runUpdate = $appUpdate->runQuery($connUpdate, $recordUpdate);

		while($rec = mysqli_fetch_assoc($runUpdate)){
			$cName = $rec['name'];
			$cStatus = $rec['status'];
		}

		$smarty->assign("nameUpdate", $cName);
		$smarty->assign("statusUpdate", $cStatus);
		$smarty->assign("check", "checked");

		$appUpdate->closeConnection($connUpdate);
	}

	if(isset($_POST['updateRec'])){
		$nameEdit = $_POST['name'];
		$statusEdit = $_POST['status'];
		$appEdit = new App();
		$connEdit = $appEdit->openConnection();
		$recordEdit = "UPDATE categories SET name='".$nameEdit."', status='".$statusEdit."' WHERE id='".$_SESSION['recId']."'";
		$runEdit = $appEdit->runQuery($connEdit, $recordEdit);
		$smarty->assign("categoryUpdated", "Category updated");
		$appEdit->closeConnection($connEdit);
	}

	$application = new App();
	$connection = $application->openConnection();
	$getAllRecords = "SELECT * FROM categories";
	$records = $application->runQuery($connection, $getAllRecords);
	$smarty->assign("records", $records);

	$smarty->assign("admin", $admin);			
	$smarty->display("admin/categories.tpl");
?>