<?php

	error_reporting(E_ALL & ~E_NOTICE);
	include_once "../includes/inc.admin.php";
	include "../includes/app/App.php";

	if(isset($_POST['addImage'])){

		$imageName = $_POST['imageName'];
		$imageCaption = $_POST['imageCaption'];
		$imageStatus = $_POST['status'];

		$imageNameError = '';
		$imageCaptionError = '';
		$imageStatusError = '';

		if(empty($imageName)){
			$imageNameError = 'Image name can not be empty';
			$smarty->assign("imageNameError", $imageNameError);
		}

		if(empty($imageCaption)){
			$imageCaptionError = 'Image caption can not be empty';
			$smarty->assign("imageCaptionError", $imageCaptionError);
		}

		if(empty($imageStatus)){
			$imageStatusError = 'Image status must be checked';
			$smarty->assign("imageStatusError", $imageStatusError);
		}

		$fileName = basename($_FILES['image']['name']);
		$fileTempName = $_FILES['image']['tmp_name'];
		$fileSize = $_FILES['image']['size'];

		$location = "C:\\xampp\htdocs\develop.blog\uploads\\slider\\".$fileName;
		$fileType = strtolower(pathinfo($location, PATHINFO_EXTENSION));

		if($fileSize > 500000){
			$fileSizeError = 'File is to large';
			$smarty->assign("fileSizeError", $fileSizeError);
		}

		if(file_exists($location)){
			$fileError = 'File already exists';
			$smarty->assign("fileError", $fileError);
		}

		if($fileType != 'jpg' && $fileType != 'png' && $fileType != 'jpeg'){
			$fileTypeError = 'Only jpg, png and jpeg files are allowed';
			$smarty->assign("fileTypeError", $fileTypeError);
		} else {
			$appInsert = new App();
			$connInsert = $appInsert->openConnection();
			$insertSql = "INSERT INTO sliders(name, caption, status, url, filename) 
						  VALUES('".$imageName."', '".$imageCaption."', '".$imageStatus."', '".$location."', '".$fileName."')";
			$runInsert = $appInsert->runQuery($connInsert, $insertSql);
			$appInsert->closeConnection($connInsert);
			move_uploaded_file($fileTempName, $location);
			$smarty->assign("message", "Slider image created");
		}

	}

	if(isset($_POST['deleteSlider'])){
		$sliderId = $_POST['sliderId'];
		$appDelete = new App();
		$connDelete = $appDelete->openConnection();
		$deleteSql = "DELETE FROM sliders WHERE id='".$sliderId."'";
		$runDeleteSql = $appDelete->runQuery($connDelete, $deleteSql);
		$smarty->assign("sliderDeleted", "Slider deleted");
		$appDelete->closeConnection($connDelete);
	}

	if(isset($_POST['updateSlider'])){
		$sliderId = $_POST['sliderId'];
		$_SESSION['sliderId'] = $sliderId;

		$appUpdate = new App();
		$connUpdate = $appUpdate->openConnection();
		$updateSql = "SELECT * FROM sliders WHERE id='".$sliderId."'";
		$runUpdate = $appUpdate->runQuery($connUpdate, $updateSql);

		while($record = mysqli_fetch_object($runUpdate)){
			$sName = $record->name;
			$sCaption = $record->caption;
			$sStatus = $record->status;
		}

		$smarty->assign("sName", $sName);
		$smarty->assign("sCaption", $sCaption);
		$smarty->assign("sStatus", $sStatus);
		$smarty->assign("check", "checked");

	}

	if(isset($_POST['editSlider'])){
		$sliderId = $_SESSION['sliderId'];

		$sliderNameData = $_POST['imageName'];
		$sliderCaptionData = $_POST['imageCaption'];
		$sliderStatusData = $_POST['status'];

		$appEdit = new App();
		$connEdit = $appEdit->openConnection();
		$editSql = "UPDATE sliders SET name='".$sliderNameData."', caption='".$sliderCaptionData."', status='".$sliderStatusData."' WHERE id='".$sliderId."'";
		$runEdit = $appEdit->runQuery($connEdit, $editSql);
		$smarty->assign("sliderEdited", "Slider updated");
		$appEdit->closeConnection($connEdit);
	}

	$appShow = new App();
	$connShow = $appShow->openConnection();
	$showSql = "SELECT * FROM sliders";
	$runShow = $appShow->runQuery($connShow, $showSql);
	$smarty->assign("sliders", $runShow);
	$appShow->closeConnection($connShow);

	$smarty->assign("admin", $admin);			
	$smarty->display("admin/sliders.tpl");

?>