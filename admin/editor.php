<?php

	error_reporting(E_ALL & ~E_NOTICE);
	include_once "../includes/inc.admin.php";
	include "../includes/app/App.php";

	if(isset($_POST['sendData'])){

		$postTitle = $_POST['title'];
		$editorData = $_POST['editorData'];
		$postStatus = $_POST['status'];

		$postTitleError = '';
		$editorDataError = '';
		$postStatusError = '';

		if(empty($postTitle)){
			$postTitleError = 'Post title can not be empty';
			$smarty->assign("postTitleError", $postTitleError);
		}

		if(empty($editorData)){
			$editorDataError = 'Post content can not be empty';
			$smarty->assign("editorDataError", $editorDataError);
		}

		if(empty($postStatus)){
			$postStatusError = 'Post status must be checked';
			$smarty->assign("postStatusError", $postStatusError);
		}

		$fileName = basename($_FILES['image']['name']);
		$fileTempName = $_FILES['image']['tmp_name'];
		$fileSize = $_FILES['image']['size'];
		$location = "C:\\xampp\htdocs\develop.blog\uploads\\editor\\".$fileName;
		$fileType = strtolower(pathinfo($location, PATHINFO_EXTENSION));

		if($fileSize > 500000){
			$fileSizeError = 'File is to large';
			$smarty->assign("fileSizeError", $fileSizeError);
		}

		if(file_exists($location)){
			$fileError = 'File already exists';
			$smarty->assign("fileError", $fileError);
		}

		if($fileType != 'jpg' && $fileType != 'png' && $fileType != 'jpeg'){
			$fileTypeError = 'Only jpg, png and jpeg files are allowed';
			$smarty->assign("fileTypeError", $fileTypeError);
		} else {
			$userId = $logged_user->getUserId();
			$appInsert = new App();
			$connInsert = $appInsert->openConnection();
			$insertSql = "INSERT INTO editorposts(user_id, title, content, status, image, created) 
						  VALUES('".$userId."', '".$postTitle."', '".$editorData."', '".$postStatus."', '".$fileName."', CURTIME())";
			$runInsert = $appInsert->runQuery($connInsert, $insertSql);
			$smarty->assign("message", "Post created");
			move_uploaded_file($fileTempName, $location);
			$appInsert->closeConnection($connInsert);
		}

	}

	if(isset($_POST['deletePost'])){

		$postId = $_POST['postId'];

		$appDelete = new App();
		$connDelete = $appDelete->openConnection();
		$deleteSql = "DELETE FROM editorposts WHERE id='".$postId."'";
		$runDelete = $appDelete->runQuery($connDelete, $deleteSql);
		$smarty->assign("postDeleted", "Post deleted");
		$appDelete->closeConnection($connDelete);

	}

	if(isset($_POST['updatePost'])){

		$postId = $_POST['postId'];
		$_SESSION['postId'] = $postId;

		$appUpdate = new App();
		$connUpdate = $appUpdate->openConnection();
		$updateSql = "SELECT * FROM editorposts WHERE id='".$postId."'";
		$runUpdate = $appUpdate->runQuery($connUpdate, $updateSql);

		while($post = mysqli_fetch_object($runUpdate)){
			$pTitle = $post->title;
			$pContent = $post->content;
			$pStatus = $post->status;
		}

		$smarty->assign("pTitle", $pTitle);
		$smarty->assign("pContent", $pContent);
		$smarty->assign("pStatus", $pStatus);
		$smarty->assign("check", "checked");

		$appUpdate->closeConnection($connUpdate);

	}

	if(isset($_POST['editData'])){

		$postId = $_SESSION['postId'];

		$postTitleData = $_POST['title'];
		$postContentData = $_POST['editorData'];
		$postStatusData = $_POST['status'];

		$appEdit = new App();
		$connEdit = $appEdit->openConnection();
		$editSql = "UPDATE editorposts SET title='".$postTitleData."', content='".$postContentData."', status='".$postStatusData."', created=CURTIME() WHERE id='".$postId."'";
		$runEdit = $appEdit->runQuery($connEdit, $editSql);
		$smarty->assign("postUpdated", "Post updated");
		$appEdit->closeConnection($connEdit);

	}

	$allEditorPosts = new App();
	$connection = $allEditorPosts->openConnection();
	$sql = "SELECT * FROM editorposts";
	$postsData = $allEditorPosts->runQuery($connection, $sql);
	$smarty->assign("postsData", $postsData);
	$allEditorPosts->closeConnection($connection);

	$getCategories = new App();
	$conn = $getCategories->openConnection();
	$categorySql = "SELECT * FROM categories";
	$allCategories = $getCategories->runQuery($conn, $categorySql);
	$smarty->assign("allCategories", $allCategories);
	$getCategories->closeConnection($conn);

	$smarty->assign("admin", $admin);			
	$smarty->display("admin/editor.tpl");

?>