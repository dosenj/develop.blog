{include file="admin/a_header.tpl"}
	<div class="category-content">
		<div class="category-form">
			<form method="POST" action="pages.php">
				<div>
					<input type="text" name="title" placeholder="Enter page title..." value="{$pTitle}">
					<div>
						{if !empty($pageTitleError)}
							<font color="red">{$pageTitleError}</font>
						{/if}
					</div>
				</div>
				<div>
					<input type="text" name="content" placeholder="Enter page content..." value="{$pContent}">
					<div>
						{if !empty($pageContentError)}
							<font color="red">{$pageContentError}</font>
						{/if}
					</div>
				</div>
				<div>
					<input type="radio" name="status" value="true" {if $pStatus == 'true'}{$check}{/if}>Published
				</div>
				<div>
					<input type="radio" name="status" value="false" {if $pStatus == 'false'}{$check}{/if}>Not published
					<div>
						{if !empty($pageStatusError)}
							<font color="red">{$pageStatusError}</font>
						{/if}
					</div>
				</div>
				<input type="submit" name="addPage" value="ADD" style="margin-top: 20px;"><input type="submit" name="editPage" value="UPDATE DATA" style="margin-left: 20px; margin-top: 20px;">
				{if !empty($pageCreated)}
					<font color="green">{$pageCreated}</font>
				{/if}
				{if !empty($delMessage)}
					<font color="red">{$delMessage}</font>
				{/if}
				{if !empty($pageUpdated)}
					<font color="green">{$pageUpdated}</font>
				{/if}
			</form>
		</div>
	</div>

	<div class="display-pages">
		<table style="width: 100%;" border="1">
			<tr>
				<th bgcolor="lightgrey">ID</th>
				<th bgcolor="lightgrey">TITLE</th>
				<th bgcolor="lightgrey">CONTENT</th>
				<th bgcolor="lightgrey">PUBLISHED</th>
				<th bgcolor="lightgrey">CREATED AT</th>
				<th bgcolor="lightgrey">ACTIONS</th>
			</tr>
			{while $page = mysqli_fetch_assoc($pages)}
				<tr>
					<td>{$page['id']}</td>
					<td>{$page['title']}</td>
					<td>{$page['content']}</td>
					<td>{$page['status']}</td>
					<td>{$page['page_time']}</td>
					<td>
						<form method="POST" action="pages.php">
							<input type="hidden" name="pageId" value="{$page['id']}">
							<input type="submit" name="deletePage" value="DELETE" style="background-color: red;">
						</form>
						<form method="POST" action="pages.php">
							<input type="hidden" name="pageId" value="{$page['id']}">
							<input type="submit" name="updatePage" value="UPDATE" style="background-color: blue;">
						</form>
					</td>
				</tr>
			{/while}
		</table>
	</div>
{include file="admin/a_footer.tpl"}