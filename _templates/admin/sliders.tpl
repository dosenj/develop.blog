{include file="admin/a_header.tpl"}
	<div class="category-content">
		<div class="category-form">
			<form method="POST" action="sliders.php" enctype="multipart/form-data">
				<div>
					<input type="text" name="imageName" placeholder="Enter image name..." value="{$sName}">
					<div>
						{if !empty($imageNameError)}
							<font color="red">{$imageNameError}</font>
						{/if}
					</div>
				</div>
				<div>
					<input type="text" name="imageCaption" placeholder="Enter image caption..." value="{$sCaption}">
					<div>
						{if !empty($imageCaptionError)}
							<font color="red">{$imageCaptionError}</font>
						{/if}
					</div>
				</div>
				<div>
					<input type="radio" name="status" value="true" {if $sStatus == 'true'}{$check}{/if}>Published
				</div>
				<div>
					<input type="radio" name="status" value="false" {if $sStatus == 'false'}{$check}{/if}>Not published
					<div>
						{if !empty($imageStatusError)}
							<font color="red">{$imageStatusError}</font> 
						{/if}
					</div>
				</div>
				<div>
					<input type="file" name="image">
					<div>
						{if !empty($fileSizeError)}
							<font color="red">{$fileSizeError}</font>
						{/if}
						{if !empty($fileError)}
							<font color="red">{$fileError}</font>
						{/if}
						{if !empty($fileTypeError)}
							<font color="red">{$fileTypeError}</font>
						{/if}
					</div>
				</div>
				<input type="submit" name="addImage" value="ADD" style="margin-top: 20px;"><input type="submit" name="editSlider" value="UPDATE DATA" style="margin-left: 20px;">
				{if !empty($message)}
					<font color="green">{$message}</font>
				{/if}
				{if !empty($sliderDeleted)}
					<font color="red">{$sliderDeleted}</font>
				{/if}
				{if !empty($sliderEdited)}
					<font color="green">{$sliderEdited}</font>
				{/if}
			</form>
		</div>
	</div>

	<div class="display-sliders">
		<table style="width: 100%;" border="1">
			<tr>
				<th bgcolor="lightgrey">ID</th>
				<th bgcolor="lightgrey">NAME</th>
				<th bgcolor="lightgrey">DESCRIPTION</th>
				<th bgcolor="lightgrey">PUBLISHED</th>
				<th bgcolor="lightgrey">IMAGE</th>
				<th bgcolor="lightgrey">ACTIONS</th>
			</tr>
			{while $slider = mysqli_fetch_object($sliders)}
				<tr>
					<td>{$slider->id}</td>
					<td>{$slider->name}</td>
					<td>{$slider->caption}</td>
					<td>{$slider->status}</td>
					<td>
						<img src="/uploads/slider/{$slider->filename}" width="40px" height="40px">
					</td>
					<td>
						<form method="POST" action="sliders.php">
							<input type="hidden" name="sliderId" value="{$slider->id}">
							<input type="submit" name="deleteSlider" value="DELETE" style="background-color: red;">
						</form>
						<form method="POST" action="sliders.php">
							<input type="hidden" name="sliderId" value="{$slider->id}">
							<input type="submit" name="updateSlider" value="UPDATE" style="background-color: blue;">
						</form>
					</td>
				</tr>
			{/while}
		</table>
	</div>
{include file="admin/a_footer.tpl"}  