{include file="admin/a_header.tpl"}
	<div class="category-content">
		<div class="category-form">
			<form method="POST" action="images.php" enctype="multipart/form-data">
				<div>
					<input type="text" name="filename" placeholder="Enter image name.." value="{$iName}">
					<div>
						{if !empty($imageNameError)}
							<font color="red">{$imageNameError}</font>
						{/if}
					</div>
				</div>
				<div>
					<input type="text" name="description" placeholder="Enter image description..." value="{$iDescription}">
					<div>
						{if !empty($imageDescriptionError)}
							<font color="red">{$imageDescriptionError}</font>
						{/if}
					</div>
				</div>
				<div>
					<input type="radio" name="status" value="true" {if $iStatus == 'true'}{$check}{/if}>Published
				</div>
				<div>
					<input type="radio" name="status" value="false" {if $iStatus == 'false'}{$check}{/if}>Not published
					<div>
						{if !empty($imageStatusError)}
							<font color="red">{$imageStatusError}</font>
						{/if}
					</div>
				</div>
				<div style="margin-top: 10px; margin-bottom: 10px;">
					<select name="selectedAlbum">
						{while $album = mysqli_fetch_object($albums)}
							<option value="{$album->id}" {if !empty($albumName) && $albumName == $album->name}{$select}{/if}>{$album->name}</option>
						{/while}
					</select>
				</div>
				<div>
					<input type="file" name="image">
					<div>
						{if !empty($fileSizeError)}
							<font color="red">{$fileSizeError}</font>
						{/if}
						{if !empty($fileError)}
							<font color="red">{$fileError}</font>
						{/if}
						{if !empty($fileTypeError)}
							<font color="red">{$fileTypeError}</font>
						{/if}
					</div>
				</div>
				<input type="submit" name="addImageData" value="ADD" style="margin-top: 15px;"><input type="submit" name="editImageData" value="UPDATE DATA" style="margin-left: 20px;">
				{if !empty($message)}
					<font color="green">{$message}</font>
				{/if}
				{if !empty($imageDeleted)}
					<font color="red">{$imageDeleted}</font>
				{/if}
				{if !empty($infoMessage)}
					<font color="green">{$infoMessage}</font>
				{/if}
			</form>
		</div>
	</div>

	<div class="display-images">
		<table style="width: 100%;" border="1">
			<tr>
				<th bgcolor="lightgrey">ID</th>
				<th bgcolor="lightgrey">NAME</th>
				<th bgcolor="lightgrey">DESCRIPTION</th>
				<th bgcolor="lightgrey">PUBLISHED</th>
				<th bgcolor="lightgrey">IMAGE</th>
				<th bgcolor="lightgrey">ACTIONS</th>
			</tr>
			{while $image = mysqli_fetch_object($images)}
				<tr>
					<td>{$image->id}</td>
					<td>{$image->name}</td>
					<td>{$image->description}</td>
					<td>{$image->status}</td>
					<td>
						<img src="/uploads/images/{$image->image}" width="40px" height="40px">
					</td>
					<td>
						<form method="POST" action="images.php">
							<input type="hidden" name="imageId" value="{$image->id}">
							<input type="submit" name="deleteImageData" value="DELETE" style="background-color: red;">
						</form>
						<form method="POST" action="images.php">
							<input type="hidden" name="imageId" value="{$image->id}">
							<input type="submit" name="updateImageData" value="UPDATE" style="background-color: blue;">
						</form>
					</td>
				</tr>
			{/while}
		</table>
	</div>
{include file="admin/a_footer.tpl"} 