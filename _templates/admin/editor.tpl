{include file="admin/a_header.tpl"}
	<div class="editor-container">
		<form method="POST" action="editor.php" enctype="multipart/form-data">
			<div>
				<input type="text" name="title" placeholder="Enter post title" value="{$pTitle}">
				<div>
					{if !empty($postTitleError)}
						<font color="red">{$postTitleError}</font>
					{/if}
				</div>
			</div>
			<div style="margin-top: 20px;">
				<textarea name="editorData" id="editor">{$pContent}</textarea>
				<div>
					{if !empty($editorDataError)}
						<font color="red">{$editorDataError}</font>
					{/if}
				</div>
			</div>
			<div>
				<input type="radio" name="status" value="true" {if $pStatus == 'true'}{$check}{/if}>Published
			</div>
			<div>
				<input type="radio" name="status" value="false" {if $pStatus == 'false'}{$check}{/if}>Not published
				<div>
					{if !empty($postStatusError)}
						<font color="red">{$postStatusError}</font>
					{/if}
				</div>
			</div>
			<div>
				<input type="file" name="image">
				<div>
					{if !empty($fileSizeError)}
						<font color="red">{$fileSizeError}</font>
					{/if}
					{if !empty($fileError)}
						<font color="red">{$fileError}</font>
					{/if}
					{if !empty($fileTypeError)}
						<font color="red">{$fileTypeError}</font>
					{/if}
				</div>
			</div>
			<input type="submit" name="sendData" value="ADD POST" style="margin-top: 20px;"><input type="submit" name="editData" value="UPDATE DATA" style="margin-top: 20px; margin-left: 20px;">
			<div>
				{if !empty($message)}
					<font color="green">{$message}</font>
				{/if}
				{if !empty($postDeleted)}
					<font color="red">{$postDeleted}</font>
				{/if}
				{if !empty($postUpdated)}
					<font color="green">{$postUpdated}</font>
				{/if}
			</div>
		</form>
	</div>

	<div class="display-editorposts">
		<table style="width: 100%;" border="1">
			<tr>
				<th bgcolor="lightgrey">ID</th>
				<th bgcolor="lightgrey">TITLE</th>
				<th bgcolor="lightgrey">CONTENT</th>
				<th bgcolor="lightgrey">PUBLISHED</th>
				<th bgcolor="lightgrey">IMAGE</th>
				<th bgcolor="lightgrey">POST DATE</th>
				<th bgcolor="lightgrey">ACTIONS</th>
			</tr>
			{while $record = mysqli_fetch_object($postsData)}
				<tr>
					<td>{$record->id}</td>
					<td>{$record->title}</td>
					<td>{$record->content}</td>
					<td>{$record->status}</td>
					<td>
						<img src="/uploads/editor/{$record->image}" width="40px" height="40px">
					</td>
					<td>{$record->created}</td>
					<td>
						<form method="POST" action="editor.php">
							<input type="hidden" name="postId" value="{$record->id}">
							<input type="submit" name="deletePost" value="DELETE" style="background-color: red;">
						</form>
						<form method="POST" action="editor.php">
							<input type="hidden" name="postId" value="{$record->id}">
							<input type="submit" name="updatePost" value="UPDATE" style="background-color: blue;">
						</form>
					</td>
				</tr>
			{/while}
		</table>
	</div>

	<script type="text/javascript">
		CKEDITOR.replace('editor'); 
	</script>
{include file="admin/a_footer.tpl"}