{include file="admin/homeland_header.tpl"}
    <div class="container">
        <div class="row">
            <div class="col-sm">
            	  <nav class="navbar navbar-expand-lg navbar-light bg-light">
            			 <a class="navbar-brand" href="#">BisLite</a>
            			 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              			   <span class="navbar-toggler-icon"></span>
            			 </button>
            			 <div class="collapse navbar-collapse" id="navbarNav">
              			   <ul class="navbar-nav">
                				  <li class="nav-item active">
                  				    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                				  </li>
                				  {while $link = mysqli_fetch_object($links)}
                					     <li class="nav-item">
                  					       <a class="nav-link" href="#">{$link->title}</a>
                					     </li>
                				  {/while}
              			   </ul>
            			 </div>
            	  </nav>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm">
            	  <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
            			   <div class="carousel-inner">
              			     <div class="carousel-item active">
                				      <img class="d-block w-100" src="/uploads/slider/download1.jpg" alt="First slide" width="100%" height="500px">
              			     </div>
              			     {while $img = mysqli_fetch_object($images)}
              				        <div class="carousel-item">
                					         <img class="d-block w-100" src="/uploads/slider/{$img->filename}" alt="{$img->caption}" width="100%" height="500px">
              				        </div>
              			     {/while}
            			   </div>
            			   <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
              			     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              			     <span class="sr-only">Previous</span>
            			   </a>
            			   <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
              			     <span class="carousel-control-next-icon" aria-hidden="true"></span>
              			     <span class="sr-only">Next</span>
            			   </a>
            	  </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm">
                <div class="card-group">
                    {while $post = mysqli_fetch_object($posts)}
                        <div class="card">
                            <img class="card-img-top" src="/uploads/{$post->image}" alt="{$post->title}" width="400px" height="400px">
                            <div class="card-body">
                                <h5 class="card-title">{$post->title}</h5>
                                <p class="card-text">{$post->content}</p>
                                <p class="card-text"><small class="text-muted">{$post->post_time}</small></p>
                            </div>
                        </div>
                    {/while}
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm">
                <hr />
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm">
                <div class="card-group">
                    {while $record = mysqli_fetch_object($allImages)}
                        <div class="card">
                            <img class="card-img-top" src="/uploads/images/{$record->image}" alt="{$record->name}" width="300px" height="300px">
                        </div>
                    {/while}
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm">
                <hr />
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm">
                <div class="card-group">
                    {while $postRecord = mysqli_fetch_object($editorPosts)}
                        <div class="card">
                            <img class="card-img-top" src="/uploads/editor/{$postRecord->image}" alt="{$postRecord->title}" width="400px" height="400px">
                            <div class="card-body">
                                <h5 class="card-title">{$postRecord->title}</h5>
                                <p class="card-text">{$postRecord->content}</p>
                                <p class="card-text"><small class="text-muted">{$postRecord->created}</small></p>
                            </div>
                        </div>
                    {/while}
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="background-color: lightgrey;">
        <div class="row">
            <div class="col-sm"> 
                <h5>ABOUT US</h5>
                    <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                    </p>
            </div>
            <div class="col-sm"> 
                <h5>EXPLORE</h5>
                <p><a href="#">Home</a></p>
                <p><a href="#">About us</a></p>
                <p><a href="#">Services</a></p>
                <p><a href="#">Portfolio</a></p>
                <p><a href="#">Blog</a></p>
            </div>
            <div class="col-sm">
                 <h5>BROWSE</h5>
                 <p><a href="#">Careers</a></p>
                 <p><a href="#">Press and Media</a></p>
                 <p><a href="#">Contact us</a></p>
                 <p><a href="#">Terms of service</a></p>
                 <p><a href="#">Privacy Policy</a></p>
            </div>
            <div class="col-sm">
                <h5>CONTACT US</h5>
                <p><b>BisLite Inc.</b><br />Always Street 265-125 Canada</p>
                <p>Phone: 987-4510-456<br />Fax: 987-4510-456</p>
            </div>
            <div class="col-sm">
                <h5>CONNECT WITH US</h5>
                <a href="#" class="fa fa-facebook"></a>
                <a href="#" class="fa fa-twitter"></a>
                <a href="#" class="fa fa-google"></a>
                <a href="#" class="fa fa-linkedin"></a>
                <a href="#" class="fa fa-youtube"></a>
                <a href="#" class="fa fa-instagram"></a>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-sm">
                <p>&copy; Copyright 2012.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                </p>
            </div> 
        </div>
    </div>
{include file="admin/homeland_footer.tpl"}     