{include file="admin/a_header.tpl"}
	<div class="category-content">
		<div class="category-form">
			<form method="POST" action="albums.php">
				<div>
					<input type="text" name="albumName" placeholder="Enter album name..." value="{$aName}">
					<div>
						{if !empty($albumNameError)}
							<font color="red">{$albumNameError}</font>
						{/if}
					</div>
				</div>
				<div>
					<input type="text" name="albumDescription" placeholder="Enter album description..." value="{$aDescription}">
					<div>
						{if !empty($albumDescriptionError)}
							<font color="red">{$albumDescriptionError}</font>
						{/if}
					</div>
				</div>
				<div>
					<input type="radio" name="status" value="true" {if $aStatus == 'true'}{$check}{/if}>Published
				</div>
				<div>
					<input type="radio" name="status" value="false" {if $aStatus == 'false'}{$check}{/if}>Not published
					<div>
						{if !empty($statusError)}
							<font color="red">{$statusError}</font>
						{/if}
					</div>
				</div>
				<input type="submit" name="addAlbum" value="ADD" style="margin-top: 20px;"><input type="submit" name="editAlbum" value="UPDATE DATA" style="margin-left: 20px; margin-top: 20px;">
				{if !empty($albumCreated)}
					<font color="green">{$albumCreated}</font>
				{/if}
				{if !empty($albumDeleted)}
					<font color="red">{$albumDeleted}</font>
				{/if}
				{if !empty($albumUpdated)}
					<font color="green">{$albumUpdated}</font>
				{/if}
			</form>
		</div>
	</div>

	<div class="display-albums">
		<table style="width: 100%;" border="1">
			<tr>
				<th bgcolor="lightgrey">ID</th>
				<th bgcolor="lightgrey">NAME</th>
				<th bgcolor="lightgrey">DESCRIPTION</th>
				<th bgcolor="lightgrey">PUBLISHED</th>
				<th bgcolor="lightgrey">ACTIONS</th>
			</tr>
			{while $album = mysqli_fetch_object($albums)}
				<tr>
					<td>{$album->id}</td>
					<td>{$album->name}</td>
					<td>{$album->description}</td>
					<td>{$album->status}</td>
					<td>
						<form method="POST" action="albums.php">
							<input type="hidden" name="albumId" value="{$album->id}">
							<input type="submit" name="deleteAlbum" value="DELETE" style="background-color: red;">
						</form>
						<form method="POST" action="albums.php">
							<input type="hidden" name="albumId" value="{$album->id}">
							<input type="submit" name="updateAlbum" value="UPDATE" style="background-color: blue;">
						</form>
					</td>
				</tr>
			{/while}
		</table>
	</div>
{include file="admin/a_footer.tpl"}