{include file="admin/a_header.tpl"}
	<div class="category-content">
		<div class="category-form">
			<form method="POST" action="categories.php">
				<div>
					<input type="text" name="name" placeholder="Enter category name..." value="{$nameUpdate}">
					<div>
						{if !empty($error)}
							<font color="red">{$error}</font>
						{/if}
					</div>
				</div>
				<div>
					<input type="radio" name="status" value="true" {if $statusUpdate=="true"}{$check}{/if}>Published
				</div>
				<div>
					<input type="radio" name="status" value="false" {if $statusUpdate=="false"}{$check}{/if}>Not published
				</div>
				<input type="submit" name="add" value="ADD" style="margin-top: 20px;"><input type="submit" name="updateRec" value="UPDATE DATA" style="margin-left: 20px; margin-top: 20px;">
				{if !empty($connError)}
					<font color="red">{$connError}</font>
				{/if}
				{if !empty($message)}
					<font color="green">{$message}</font>
				{/if}
				{if !empty($categoryDeleted)}
					<font color="red">{$categoryDeleted}</font>
				{/if}
				{if !empty($categoryUpdated)}
					<font color="green">{$categoryUpdated}</font>
				{/if}
			</form>
		</div>
	</div>

	<div class="display-categories">
		<table style="width: 100%;" border="1">
			<tr>
				<th bgcolor="lightgrey">ID</th>
				<th bgcolor="lightgrey">NAME</th>
				<th bgcolor="lightgrey">PUBLISHED</th>
				<th bgcolor="lightgrey">ACTIONS</th>
			</tr>
			{while $record = mysqli_fetch_assoc($records)}
				<tr>
					<td>{$record['id']}</td>
					<td>{$record['name']}</td>
					<td>{$record['status']}</td>
					<td>
						<form method="POST" action="categories.php">
							<input type="hidden" name="recordId" value="{$record['id']}">
							<input type="submit" name="delete" value="DELETE" style="background-color: red;">
						</form>
						<form method="POST" action="categories.php">
							<input type="hidden" name="recordId" value="{$record['id']}">
							<input type="submit" name="update" value="UPDATE" style="background-color: blue;">
						</form>
					</td>
				</tr>
			{/while}
		</table>
	</div>
{include file="admin/a_footer.tpl"}