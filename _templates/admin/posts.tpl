{include file="admin/a_header.tpl"}
	<div class="category-content">
		<div class="category-form">
			<form method="POST" action="posts.php" enctype="multipart/form-data">
				<div>
					<input type="text" name="title" placeholder="Enter post title..." value="{$postTitleUpdate}">
					<div>
						{if !empty($titleError)}
							<font color="red">{$titleError}</font>
						{/if}
					</div>
				</div>
				<div>
					<input type="text" name="content" placeholder="Enter post content..." value="{$postContentUpdate}">
					<div>
						{if !empty($contentError)}
							<font color="red">{$contentError}</font>
						{/if}
					</div>
				</div>
				<div>
					<input type="radio" name="status" value="true" {if $postStatusUpdate == 'true'}{$check}{/if}>Published
				</div>
				<div>
					<input type="radio" name="status" value="false" {if $postStatusUpdate == 'false'}{$check}{/if}>Not published
				</div>
				<div>
					<input type="file" name="image">
					<div>
						{if !empty($fileError)}
							<font color="red">{$fileError}</font>
						{/if}
						{if !empty($fileTypeError)}
							<font color="red">{$fileTypeError}</font>
						{/if}
						{if !empty($fileSizeError)}
							<font color="red">{$fileSizeError}</font>
						{/if}
					</div>
				</div>
				<input type="submit" name="addPost" value="ADD POST" style="margin-top: 20px;"><input type="submit" name="editPostData" value="UPDATE DATA" style="margin-left: 20px;">
				{if !empty($message)}
					<font color="green">{$message}</font>
				{/if}
				{if !empty($postDeleted)}
					<font color="red">{$postDeleted}</font>
				{/if}
				{if !empty($info)}
					<font color="green">{$info}</font>
				{/if}
			</form>
		</div>
	</div>

	<div class="display-posts">
		<table style="width: 100%;" border="1">
			<tr>
				<th bgcolor="lightgrey">ID</th>
				<th bgcolor="lightgrey">TITLE</th>
				<th bgcolor="lightgrey">CONTENT</th>
				<th bgcolor="lightgrey">PUBLISHED</th>
				<th bgcolor="lightgrey">IMAGE</th>
				<th bgcolor="lightgrey">POST DATE</th>
				<th bgcolor="lightgrey">POST TIME</th>
				<th bgcolor="lightgrey">ACTIONS</th>
			</tr>
			{while $post = mysqli_fetch_assoc($posts)}
				<tr>
					<td>{$post['id']}</td>
					<td>{$post['title']}</td>
					<td>{$post['content']}</td>
					<td>{$post['status']}</td>
					<td><img src="/uploads/{$post['image']}" width="40px" height="40px"></td>
					<td>{$post['post_date']}</td>
					<td>{$post['post_time']}</td>
					<td>
						<form method="POST" action="posts.php">
							<input type="hidden" name="postId" value="{$post['id']}">
							<input type="submit" name="deletePost" value="DELETE" style="background-color: red;">
						</form>
						<form method="POST" action="posts.php">
							<input type="hidden" name="postId" value="{$post['id']}">
							<input type="submit" name="updatePost" value="UPDATE" style="background-color: blue;">
						</form>
					</td>
				</tr>
			{/while}
		</table>
	</div>
{include file="admin/a_footer.tpl"}